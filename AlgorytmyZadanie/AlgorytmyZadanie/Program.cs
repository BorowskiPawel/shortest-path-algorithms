﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorytmyZadanie.Models;

namespace AlgorytmyZadanie
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopWatch3 = new Stopwatch();
            stopWatch3.Start();
            var path = @"/users/user/repozytoria/algorytmyzadanierepo/algorytmyzadanie/algorytmyzadanie/in4.txt";
            var fileLoader = new FileLoader();
            var fileText = fileLoader.LoadTextFile(path);
            stopWatch3.Stop();
            TimeSpan ts3 = stopWatch3.Elapsed;
            Console.WriteLine("File .txt loaded" + ts3.ToString());


            Stopwatch stopWatch2 = new Stopwatch();
            stopWatch2.Start();
            var incidenceList = fileLoader.CreateIncidenceList(fileText);
            stopWatch2.Stop();
            TimeSpan ts2 = stopWatch2.Elapsed;
            Console.WriteLine("Incidence list loaded" + ts2.ToString());


            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            var dijkstraResult = new Algorithms().Dijkstra(incidenceList);
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            Console.WriteLine("Dijkstra time elapsed: " + ts.ToString());


            Stopwatch stopwatch4 = new Stopwatch();
            stopwatch4.Start();
            var bfsResult = new Algorithms().Bfs(incidenceList);
            stopwatch4.Stop();
            TimeSpan ts4 = stopwatch4.Elapsed;
            Console.WriteLine("Bfs time elapsed: " + ts4.ToString());


            StringBuilder sb = new StringBuilder();
            sb.Append(dijkstraResult.Distance + Environment.NewLine);
            foreach(var node in dijkstraResult.Path)
            {
                sb.Append(node + " " );
            }


            sb.Append(Environment.NewLine + bfsResult.TransitionNodesCount + " ");
            sb.Append(bfsResult.PathDistance + Environment.NewLine);
            foreach (var node in bfsResult.Path)
            {
                sb.Append(node + " ");
            }
            sb.Append(Environment.NewLine);


            File.WriteAllText("./out.txt", sb.ToString());


        }
    }
}
