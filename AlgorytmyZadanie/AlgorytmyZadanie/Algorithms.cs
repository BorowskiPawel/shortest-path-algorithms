﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorytmyZadanie.Models;
using Priority_Queue;

namespace AlgorytmyZadanie
{
    public class Algorithms
    {

        public BfsResult Bfs(IncidenceList incidenceList)
        {
            var visitedTable = new bool[incidenceList.GraphNodesCount+1];
            var distanceTable = new int[incidenceList.GraphNodesCount+1];
            var parentTable = new int[incidenceList.GraphNodesCount+1];
            var queue = new Queue<Node>();

            foreach (var node in incidenceList.Nodes)
            {
                visitedTable[node.NodeId] = false;
                distanceTable[node.NodeId] = 99999;
            }

            visitedTable[incidenceList.NodeFrom] = true;
            distanceTable[incidenceList.NodeFrom] = 0;
            queue.Enqueue(incidenceList.Nodes[incidenceList.NodeFrom]);

            while(queue.Count >0)
            {
                var v = queue.Peek();
                queue.Dequeue();
                foreach(var neighbour in v.NeighborsNodes)
                {
                    if(visitedTable[neighbour.Id] == false)
                    {
                        queue.Enqueue(incidenceList.Nodes[neighbour.Id]);
                        visitedTable[neighbour.Id] = true;
                        distanceTable[neighbour.Id] = distanceTable[v.NodeId] + 1;
                        parentTable[neighbour.Id] = v.NodeId;
                    }
                }
            }




            var parent = parentTable[incidenceList.NodeTo];
            var pathList = new List<int>();
            int pathDistance = 0;

            pathList.Add(incidenceList.NodeTo);

            while (parent != incidenceList.NodeFrom)
            {
                pathDistance += incidenceList.Nodes.First(x => x.NodeId == pathList.Last())
                                .NeighborsNodes.First(z => z.Id == parent).Distance;
                pathList.Add(parent);

                parent = parentTable[parent];
            }

            pathDistance += incidenceList.Nodes.First(x => x.NodeId == pathList.Last())
                                .NeighborsNodes.First(z => z.Id == parent).Distance;
            pathList.Add(parent);
            pathList.Reverse();




            return new BfsResult()
            {
                TransitionNodesCount = pathList.Count() -2 ,
                Path = pathList,
                PathDistance = pathDistance
            };
        }

        public DijkstraResult Dijkstra(IncidenceList incidenceList)
        {

            var visitedTable = new bool[incidenceList.GraphNodesCount + 1];
            var distanceTable = new int[incidenceList.GraphNodesCount + 1];
            var parentTable = new int[incidenceList.GraphNodesCount + 1];
            var priorityQueue = new SimplePriorityQueue<int>();


            foreach (var node in incidenceList.Nodes)
            {
                if(node != null)
                {
                    visitedTable[node.NodeId] = false;
                    distanceTable[node.NodeId] = 99999;
                    if(node.NodeId != 0)
                    {
                        priorityQueue.Enqueue(node.NodeId, 99999);

                    }
                }

            }

            //avoid tab[0]
            visitedTable[0] = true;
            distanceTable[0] = 99999;

            distanceTable[incidenceList.NodeFrom] = 0;
            priorityQueue.UpdatePriority(incidenceList.NodeFrom, 0);

            while(priorityQueue.Count > 0)
            {
                int v = priorityQueue.First;
                priorityQueue.Dequeue();

                visitedTable[v] = true;
                foreach(var w in incidenceList.Nodes[v].NeighborsNodes)
                {
                    if(distanceTable[v]+ w.Distance < distanceTable[w.Id])
                    {
                        distanceTable[w.Id] = distanceTable[v] + w.Distance;
                        priorityQueue.UpdatePriority(w.Id, distanceTable[v] + w.Distance);
                        parentTable[w.Id] = v;
                    }
                }

            }


            var parent = parentTable[incidenceList.NodeTo];
            var pathList = new List<int>();
            pathList.Add(incidenceList.NodeTo);
            while (parent != incidenceList.NodeFrom)
            {
                pathList.Add(parent);
                parent = parentTable[parent];
            }
            pathList.Add(parent);
            pathList.Reverse();

            return new DijkstraResult()
            {
                 Distance = distanceTable[incidenceList.NodeTo],
                 Path = pathList
            };
        }
    }
}
