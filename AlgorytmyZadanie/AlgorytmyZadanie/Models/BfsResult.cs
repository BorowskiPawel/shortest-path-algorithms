﻿using System;
using System.Collections.Generic;

namespace AlgorytmyZadanie.Models
{
    public class BfsResult
    {
        public int TransitionNodesCount { get; set; }
        public int PathDistance { get; set; }
        public List<int> Path { get; set; }
    } 
}

