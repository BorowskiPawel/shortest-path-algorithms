using System;
using AlgorytmyZadanie.Models;
using Xunit;

namespace AlgorytmyZadanie.Tests
{
    public class FileLoaderTests
    {
        public FileLoaderTests()
        {
        }


        [Theory]
        [InlineData(@"/users/user/repozytoria/algorytmyzadanierepo/algorytmyzadanie/algorytmyzadanie/in.txt")]
        public void LoadFileTest(string path)
        {
            var result = new FileLoader().LoadTextFile(path);
            Assert.NotEmpty(result);
        }

        
        [Theory]
        [InlineData(@"/users/user/repozytoria/algorytmyzadanierepo/algorytmyzadanie/algorytmyzadanie/in.txt")]
        public void CreateIncidenceList(string path)
        {
            var fileLoader = new FileLoader();
            var fileText = fileLoader.LoadTextFile(path);
            Assert.NotEmpty(fileText);

            var result = fileLoader.CreateIncidenceList(fileText);
            Assert.NotEmpty(result.Nodes);

        }
        
    }
}
