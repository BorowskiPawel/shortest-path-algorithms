﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using AlgorytmyZadanie.Models;
using Priority_Queue;
using Xunit;

namespace AlgorytmyZadanie.Tests
{
    public class AlgorithmsTests
    {

        [Theory]
        [InlineData(@"/users/user/repozytoria/algorytmyzadanierepo/algorytmyzadanie/algorytmyzadanie/in.txt")]
        //[InlineData(@"/users/user/repozytoria/algorytmyzadanierepo/algorytmyzadanie/algorytmyzadanie/in2.txt")]
        //[InlineData(@"/users/user/repozytoria/algorytmyzadanierepo/algorytmyzadanie/algorytmyzadanie/in3.txt")]
        //[InlineData(@"/users/user/repozytoria/algorytmyzadanierepo/algorytmyzadanie/algorytmyzadanie/in4.txt")]

        public void BfsTest(string path)
        {
            var fileLoader = new FileLoader();
            var fileText = fileLoader.LoadTextFile(path);
            Assert.NotEmpty(fileText);

            var result = fileLoader.CreateIncidenceList(fileText);
            Assert.NotEmpty(result.Nodes);

            var bfsResult = new Algorithms().Bfs(result);
        }

        [Theory]
        [InlineData(@"/users/user/repozytoria/algorytmyzadanierepo/algorytmyzadanie/algorytmyzadanie/in.txt")]
        //[InlineData(@"/users/user/repozytoria/algorytmyzadanierepo/algorytmyzadanie/algorytmyzadanie/in2.txt")]
        //[InlineData(@"/users/user/repozytoria/algorytmyzadanierepo/algorytmyzadanie/algorytmyzadanie/in3.txt")]
        //[InlineData(@"/users/user/repozytoria/algorytmyzadanierepo/algorytmyzadanie/algorytmyzadanie/in4.txt")]
        public void DijkstraTest(string path)
        {
            var fileLoader = new FileLoader();
            var fileText = fileLoader.LoadTextFile(path);
            Assert.NotEmpty(fileText);

            var result = fileLoader.CreateIncidenceList(fileText);
            Assert.NotEmpty(result.Nodes);

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            var dijkstraResult = new Algorithms().Dijkstra(result);
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
        }

    }
}
