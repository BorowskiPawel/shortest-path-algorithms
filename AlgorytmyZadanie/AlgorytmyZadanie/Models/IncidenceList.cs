﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Priority_Queue;

namespace AlgorytmyZadanie.Models
{
    public class IncidenceList
    {
        public Node[] Nodes { get; set; }

        public int GraphNodesCount { get; set; }

        public int GraphEdgesCount { get; set; }

        public int NodeFrom { get; set; }

        public int NodeTo { get; set; }

    }
}
