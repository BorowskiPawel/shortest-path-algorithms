﻿using System;
using System.Collections.Generic;

namespace AlgorytmyZadanie.Models
{
    public class DijkstraResult
    {
        public int Distance { get; set; }
        public List<int> Path { get; set; }
    }
}
