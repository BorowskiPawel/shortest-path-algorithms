﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmyZadanie.Models
{
    public class NeighbourNode
    {
        public int Id { get; set; }
        public int Distance { get; set; }

        public NeighbourNode(int id, int distance)
        {
            this.Id = id;
            this.Distance = distance;
        }
    }
}
