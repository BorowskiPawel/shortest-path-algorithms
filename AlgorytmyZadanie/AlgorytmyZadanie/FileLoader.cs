﻿using AlgorytmyZadanie.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Priority_Queue;

namespace AlgorytmyZadanie
{
    public class FileLoader
    {
        public string[] LoadTextFile(string path)
        {
            string[] lines = File.ReadAllLines(path, Encoding.UTF8);
            return lines;
        }

        public IncidenceList CreateIncidenceList(string[] fileData)
        {
            //read program info
            var graphInfoLine = fileData[0].Split(' ');
            var graphNodesCount = Convert.ToInt32(graphInfoLine[0]);
            var graphEdgesCount = Convert.ToInt32(graphInfoLine[1]);

            var searchedTrackInfoLine = fileData.Last().Split(' ');
            var nodeFrom = Convert.ToInt32(searchedTrackInfoLine[0]);
            var nodeTo = Convert.ToInt32(searchedTrackInfoLine[1]);

            //converd data from file to table of nodes
            Node[] nodes = new Node[graphNodesCount+1];
            nodes[0] = new Node(0,null);
            for (var i=1 ; i<fileData.Length-1; i++) //without last and first line
            {
                var line = fileData[i];
                var numberArray = line.Split(' ');
                var nodeId = Convert.ToInt32(numberArray[0]);
                var nodeNeighbourId = Convert.ToInt32(numberArray[1]);
                var pathValue = Convert.ToInt32(numberArray[2]);
                var keyValuePair = new NeighbourNode(nodeNeighbourId, pathValue);

                if (nodes[nodeId] == null) //add new
                {
                    nodes[nodeId] =new Node(nodeId,
                        new List<NeighbourNode>(){ keyValuePair });
                }
                else //append neighbours list for node
                {
                    nodes[nodeId].NeighborsNodes
                        .Add(keyValuePair);
                }

                //add the same edge for neighbour
                if (nodes[nodeNeighbourId] == null)
                {
                    nodes[nodeNeighbourId] = new Node(nodeNeighbourId,
                        new List<NeighbourNode>() { new NeighbourNode(nodeId, pathValue) });
                }
                else
                {
                    nodes[nodeNeighbourId].NeighborsNodes
                        .Add(new NeighbourNode(nodeId, pathValue));
                }

            }

            return new IncidenceList()
            {
                Nodes = nodes,
                GraphEdgesCount = graphEdgesCount,
                GraphNodesCount = graphNodesCount,
                NodeFrom = nodeFrom,
                NodeTo = nodeTo
            };
        }
    }
}
