﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Priority_Queue;

namespace AlgorytmyZadanie.Models
{
    public class Node
    {
        public int NodeId { get; set; }

        public bool IsVisited { get; set; }


        // <nodeId, distance>
        public List<NeighbourNode> NeighborsNodes { get; set; }

        public Node(int nodeId, List<NeighbourNode> neighborsNodes)
        {
            this.NodeId = nodeId;
            this.NeighborsNodes = neighborsNodes;

            }
    }
}
